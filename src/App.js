import { gapi } from 'gapi-script';
import { useEffect } from 'react';
import './App.css';
import Login from './components/Login';
import LandingPage from './components/LandingPage'

const clientId = "50609041240-kdq66vd369c8mufm1ua3akt5sjuorqon.apps.googleusercontent.com";

function App() {

  useEffect(() => {
    function start() {
      gapi.client.init({ clientId: clientId, scope: "" })
    };
    gapi.load('client:auth2', start);
  })

  return (<>
    <LandingPage />
  </>)
}

export default App;
