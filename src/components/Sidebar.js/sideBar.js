import React from "react";
import '../style.css';

const SideBar =() => {
    return (
      <div className="sidebarcontainer">
          <div className="sidebar">
              <div className="sidebaritem-1">
                  <div className="sidebar-icon"></div>
                  <h3>Hangouts</h3>
              </div>
              <div className="sidebaritem-2">App Download</div>
              <div className="sidebaritem-3">
              <svg width="24px" height="24px" className="sidebarIcon"><path d="M6 18c0 .55.45 1 1 1h1v3.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5V19h2v3.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5V19h1c.55 0 1-.45 1-1V8H6v10zM3.5 8C2.67 8 2 8.67 2 9.5v7c0 .83.67 1.5 1.5 1.5S5 17.33 5 16.5v-7C5 8.67 4.33 8 3.5 8zm17 0c-.83 0-1.5.67-1.5 1.5v7c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5v-7c0-.83-.67-1.5-1.5-1.5zm-4.97-5.84l1.3-1.3c.2-.2.2-.51 0-.71-.2-.2-.51-.2-.71 0l-1.48 1.48C13.85 1.23 12.95 1 12 1c-.96 0-1.86.23-2.66.63L7.85.15c-.2-.2-.51-.2-.71 0-.2.2-.2.51 0 .71l1.31 1.31C6.97 3.26 6 5.01 6 7h12c0-1.99-.97-3.75-2.47-4.84zM10 5H9V4h1v1zm5 0h-1V4h1v1z"></path></svg>
              <p>Android</p>
              </div>
              <div className="sidebaritem-4">
              <svg width="24px" height="24px" className="sidebarIcon"><path d="M4 9h2V7H4v2zm0 8h2v-6H4v6zm7-10H9c-1.1 0-2 .9-2 2v6c0 1.1.9 2 2 2h2c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2zm0 8H9V9h2v6zm9-6V7h-4c-1.1 0-2 .9-2 2v2c0 1.1.9 2 2 2h2v2h-4v2h4c1.1 0 2-.9 2-2v-2c0-1.1-.9-2-2-2h-2V9h4z"></path></svg>
              <p>iOS</p>
              </div>
              <div className="sidebaritem-5">
              <svg width="24px" height="24px" className="sidebarIcon"><path d="M20 18c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2H4c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2H0v2h24v-2h-4zM4 6h16v10H4V6z"></path></svg>
              <p>Chrome</p>
              </div>
              <div className="sidebaritem-6">
                  <a href='https://policies.google.com/privacy'>Privacy</a >
                  <span>.</span>
                  <a href='https://policies.google.com/terms'>Term</a >
                  <span>.</span>
                  <a href='https://support.google.com/hangouts/answer/9334169'>Program Policy</a >
              </div>
          </div>
      </div>
    )
}

export default SideBar