import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyApFO4PJkUhMcFwaIzjAXJX-uH80XZUFf4",
  authDomain: "hangouts-app-350211.firebaseapp.com",
  databaseURL: "https://hangouts-app-350211-default-rtdb.firebaseio.com",
  projectId: "hangouts-app-350211",
  storageBucket: "hangouts-app-350211.appspot.com",
  messagingSenderId: "50609041240",
  appId: "1:50609041240:web:eddca4d61c68e780903f2d",
  measurementId: "G-C0P681GQ96"
};

firebase.initializeApp(firebaseConfig);

export default firebase;