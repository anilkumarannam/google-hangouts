import Navigation from "../Navigation"
import SideBar from "../Sidebar.js/sideBar";
import { useState } from "react";
import '../style.css';


const LandingPage = () => {
    const [displayValueOfSideBar, setDisplayValueOfSideBar] = useState(false);
    const handleMenu = () => {
        console.log('clicked ')
        setDisplayValueOfSideBar(true);
    }
    const handleMenuAndHide =(e) =>{
        console.log(e.target.id);
        if(e.target.id !== 'btn-hamburger' && e.target.id !== 'svg-hamburger')
        setDisplayValueOfSideBar(false);
    }
    const handleMessage = () => {
        console.log('message');
    }
    return (
        <div  onClick={handleMenuAndHide}>

            <div className="landing-page">

                <Navigation handleMenu={handleMenu}></Navigation>
                {displayValueOfSideBar ? <SideBar></SideBar> : <></>}

                <h1 className="heading">Talk to your friends and family</h1>
                <p className="introPara">Hangouts lets you video call or message the people you love.</p>
                <div className="videoAndMessage">
                    <div className="video">
                        <a href='https://apps.google.com/meet/?hl=en-US&authuser=0'><div className='a'>
                            <svg width="24px" height="24px" className="video-icon"><path d="M17 10.5V7c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.55 0 1-.45 1-1v-3.5l4 4v-11l-4 4z"></path></svg>
                        </div></a>
                        <p className="Vp">Video Call</p>
                    </div>

                    <div className="message" onClick={handleMessage}>
                        <div className='a' href='#'>
                            <svg width="24px" height="24px" className="message-icon"><path d="M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zM6 9h12v2H6V9zm8 5H6v-2h8v2zm4-6H6V6h12v2z"></path></svg>
                        </div>
                        <p className="Vp">Message</p>
                    </div>
                </div>
                <div className="Photo">
                    <p className="photoBy">Photo By Satyanand Yadav</p>
                </div>
            </div>
            <a href='#bottom'><div className='directToTopDown'>
                <span></span>
            </div></a>
            <div className="download-Nav">
                <span className="download">Download For</span>
                <svg width="24px" height="24px" className="android-icon"><path d="M6 18c0 .55.45 1 1 1h1v3.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5V19h2v3.5c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5V19h1c.55 0 1-.45 1-1V8H6v10zM3.5 8C2.67 8 2 8.67 2 9.5v7c0 .83.67 1.5 1.5 1.5S5 17.33 5 16.5v-7C5 8.67 4.33 8 3.5 8zm17 0c-.83 0-1.5.67-1.5 1.5v7c0 .83.67 1.5 1.5 1.5s1.5-.67 1.5-1.5v-7c0-.83-.67-1.5-1.5-1.5zm-4.97-5.84l1.3-1.3c.2-.2.2-.51 0-.71-.2-.2-.51-.2-.71 0l-1.48 1.48C13.85 1.23 12.95 1 12 1c-.96 0-1.86.23-2.66.63L7.85.15c-.2-.2-.51-.2-.71 0-.2.2-.2.51 0 .71l1.31 1.31C6.97 3.26 6 5.01 6 7h12c0-1.99-.97-3.75-2.47-4.84zM10 5H9V4h1v1zm5 0h-1V4h1v1z"></path></svg>
                <a className='linkToDownload' href="https://play.google.com/store/apps/details?id=com.google.android.talk&referrer=utm_source%3Dlandingpage%26utm_campaign%3Dlandingpage&hl=en-US">Android</a>
                <svg width="24px" height="24px" className="iOS-icon"><path d="M4 9h2V7H4v2zm0 8h2v-6H4v6zm7-10H9c-1.1 0-2 .9-2 2v6c0 1.1.9 2 2 2h2c1.1 0 2-.9 2-2V9c0-1.1-.9-2-2-2zm0 8H9V9h2v6zm9-6V7h-4c-1.1 0-2 .9-2 2v2c0 1.1.9 2 2 2h2v2h-4v2h4c1.1 0 2-.9 2-2v-2c0-1.1-.9-2-2-2h-2V9h4z"></path></svg>
                <a className='linkToDownload' href="https://play.google.com/store/apps/details?id=com.google.android.talk&referrer=utm_source%3Dlandingpage%26utm_campaign%3Dlandingpage&hl=en-US">App Store</a>
                <svg width="24px" height="24px" className="chrome-icon"><path d="M20 18c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2H4c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2H0v2h24v-2h-4zM4 6h16v10H4V6z"></path></svg>
                <a className='linkToDownload' href="https://play.google.com/store/apps/details?id=com.google.android.talk&referrer=utm_source%3Dlandingpage%26utm_campaign%3Dlandingpage&hl=en-US">Chrome</a>
            </div>
            <div className="meassageContainer">
                <div className="gifContainer">
                    <div className="forGif"></div>
                </div>
                <div className="detailsOfMessage">
                    <h1 className="messageHeading">Messaging</h1>
                    <ul>
                        <li>Have a one-on-one conversation or a group chat with the whole gang</li>
                        <li>Say even more with emoji, photos, GIFs</li>
                        <li>Sync your chats from device to device and keep the conversation going wherever you are</li>
                    </ul>
                </div>
            </div>
            <div className="videoContainer" id='bottom'>
                <div className="gifContainer">
                    <img src='https://ssl.gstatic.com/s2/oz/images/hangouts/video_ipad_screen_272.gif' alt='Video'></img>
                </div>

                <div className="detailsOfVideo">
                    <h1 className="videoHeading">Video</h1>
                    <ul>
                        <li>Turn any conversation into a free video call with just one tap</li>
                        <li>Talk one-on-one or invite friends for a group chat with up to 10 people</li>
                        <li>*Desktop only</li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default LandingPage;
