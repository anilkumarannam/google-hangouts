import React from 'react';
import '../style.css'

const Navigation = (props) => {
    
    
    return (
        <>
        <nav className='nav'>
            <div className='left-nav'>
                <svg width="24px" height="24px" className="hamburger" id='btn-hamburger' onClick={props.handleMenu}><path id='svg-hamburger' d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path></svg>
                <img src='googleHangouts.png' alt='Google Layout' className='nav-icon'></img>
            </div>
            <div className='right-nav'>
                <a href='https://gitlab.com/anilkumarannam/hangouts-app' className='nav-sign-in'>Sign in</a>
                
            </div>
        </nav>
        {/* {displayValueOfSideBar ?<SideBar></SideBar> : <></>} */}
        
        </>
    )
}

export default Navigation
