import { useEffect, useState } from "react";
import { GoogleLogin } from "react-google-login";
import InputField from '../InputField';

import './login.css';




const Login = () => {
  const [loggedIn, setLoggedIn] = useState(false);
  const onSuccess = (res) => {
    console.log("Login Successful", res);
    setLoggedIn(true);
  }

  const onFailure = (res) => {
    console.log(res);
  }
  if (!loggedIn) {
    return (
      <div className="login">
        <div className="login-form">
          <div className="login-cred">
            <h1>Hangouts App</h1>
            <InputField type="text" label="Username" id="user-name" />
            <InputField type="password" label="password" id="user-password" />
            <div className="buttons">
              <button>Login</button> <p>OR</p>
              <div id="signInButton" className="google-button">
                <GoogleLogin
                  clientId='50609041240-kdq66vd369c8mufm1ua3akt5sjuorqon.apps.googleusercontent.com'
                  buttonText="Sign in"
                  onSuccess={onSuccess}
                  onFailure={onFailure}
                  cookiePolicy={'single_host_origin'}
                  isSignedIn={true}
                  SameSite='Strict'
                />
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  } else {
    return (
      <h1>Landing Page</h1>
    );
  }
}

export default Login;

// 50609041240-kdq66vd369c8mufm1ua3akt5sjuorqon.apps.googleusercontent.com

// https://bobbyhadz.com/blog/react-module-not-found-cant-resolve-react-dom
