import { configureStore } from '@reduxjs/toolkit';
import inputFieldSlice from './inputFieldSlice';
export const store = configureStore({
  reducer: { inputFieldSlice },
});
