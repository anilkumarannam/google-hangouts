import { createSlice } from "@reduxjs/toolkit";

const formInputData = createSlice({
  name: "formInputData",
  initialState: { formData: {} },
  reducers: {
    update: (state, actions) => {
      const { id, value } = actions.payload;
      state.formData[id] = value;
    }
  }
});

export const { update } = formInputData.actions;
export const selectFormData = (state) => state.inputFieldSlice.formData;
export default formInputData.reducer;
